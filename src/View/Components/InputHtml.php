<?php
/**
 * Created by Deka Webdesign
 * User: Dirk Kokx
 * Date: 08/09/2020
 * Time: 09:26
 */

namespace Deka\Components\View\Components;


class InputHtml extends \Illuminate\View\Component
{
    public const TIPTAP = 'tiptap';
    public const SUMMERNOTE = 'summernote';

    public $label;
    public $name;
    public $type;
    public $value;
    public $showErrors;
    public $showPlain;
    public $errors;
    public $required;
    public $readOnly;

    public $summernoteConfig = [
        "height" => "200",
        "styleTags" => ['p', 'h2', 'h3', 'h4'],
        "disableDragAndDrop" => true,
        "toolbar" => [
            // [groupName, [list of button]]
            ['style', ['bold', 'italic', 'underline', 'clear']],
            // ['font', ['strikethrough', 'superscript', 'subscript']],
            // ['fontsize', ['fontsize']],
            // ['color', ['color']],
            ['para', ['style', 'ul', 'ol']],
            // ['height', ['height']],
            // ['table', ['table']],
            ['insert', ['link', 'picture', 'video']], // picture, video
            ['view', ['fullscreen', 'codeview']],
        ],
    ];

    public function __construct($name, $label, $value = '', $required = false, $showErrors = false, $errors = false, $showPlain = true, $type = self::TIPTAP, $config = false, $readOnly = false)
    {
        $this->name = $name;
        $this->label = $label;
        $this->value = $value;
        $this->showErrors = $showErrors;
        $this->required = $required;
        $this->errors = $errors;
        $this->showPlain = $showPlain;
        $this->type = $type;
        $this->readOnly = $readOnly;

        if ($config && $this->isJson($config) && $this->type === self::SUMMERNOTE) {
            $conf = json_decode($config, 1);
            foreach ($conf as $key => $item) {
                $this->summernoteConfig[$key] = $item;
            }
        }
    }

    /**
     * @inheritDoc
     */
    public function render()
    {
        if ($this->type === self::SUMMERNOTE) {
            return view('components::components.input-html-summernote');
        }

        return view('components::components.input-html');
    }

    private function isJson($string) {
        json_decode($string);
        return json_last_error() === JSON_ERROR_NONE;
    }
}
