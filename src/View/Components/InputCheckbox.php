<?php
/**
 * Created by Deka Webdesign
 * User: Dirk Kokx
 * Date: 08/09/2020
 * Time: 09:26
 */

namespace Deka\Components\View\Components;


class InputCheckbox extends \Illuminate\View\Component
{

    public $label;
    public $name;
    public $value;

    public function __construct($name, $label, $value = '')
    {
        $this->name = $name;
        $this->label = $label;
        $this->value = $value;
    }

    /**
     * @inheritDoc
     */
    public function render()
    {
        return view('components::components.input-checkbox');
    }
}
