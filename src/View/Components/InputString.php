<?php
/**
 * Created by Deka Webdesign
 * User: Dirk Kokx
 * Date: 08/09/2020
 * Time: 09:26
 */

namespace Deka\Components\View\Components;


class InputString extends \Illuminate\View\Component
{

    public $label;
    public $name;
    public $type;
    public $value;
    public $showErrors;
    public $errors;
    public $required;
    public $pid;
    public $pre;
    public $post;

    public function __construct(
        $name,
        $label,
        $value = '',
        $required = false,
        $showErrors = false,
        $errors = false,
        $pid = false,
        $pre = false,
        $post = false,
        $type = 'text'
    )
    {
        $this->name = $name;
        $this->type = $type;
        $this->pid = ($pid)? $pid : $name;
        $this->label = $label;
        $this->value = $value;
        $this->showErrors = $showErrors;
        $this->required = $required;
        $this->errors = $errors;
        $this->pre = $pre;
        $this->post = $post;
    }

    /**
     * @inheritDoc
     */
    public function render()
    {
        return view('components::components.input-string');
    }
}
