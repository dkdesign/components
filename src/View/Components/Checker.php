<?php
/**
 * Created by Deka Webdesign
 * User: Dirk Kokx
 * Date: 08/09/2020
 * Time: 09:26
 */

namespace Deka\Components\View\Components;


class Checker extends \Illuminate\View\Component
{

    public $value;

    public function __construct($value)
    {
        $this->value = $value;
    }

    /**
     * @inheritDoc
     */
    public function render()
    {
        $value = $this->value;
        return view('components::components.checker', compact('value'))->render();
    }
}
