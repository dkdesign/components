<?php
/**
 * Created by Deka Webdesign
 * User: Dirk Kokx
 * Date: 08/09/2020
 * Time: 09:26
 */

namespace Deka\Components\View\Components;


class InputText extends \Illuminate\View\Component
{

    public $label;
    public $name;
    public $value;
    public $showErrors;
    public $errors;
    public $required;

    public function __construct($name, $label, $value = '', $required = false, $showErrors = false, $errors = false)
    {
        $this->name = $name;
        $this->label = $label;
        $this->value = $value;
        $this->showErrors = $showErrors;
        $this->required = $required;
        $this->errors = $errors;
    }

    /**
     * @inheritDoc
     */
    public function render()
    {
        return view('components::components.input-text');
    }
}
