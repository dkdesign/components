<?php

namespace Deka\Components;

use Deka\Components\View\Components\Checker;
use Deka\Components\View\Components\InputCheckbox;
use Deka\Components\View\Components\InputHtml;
use Deka\Components\View\Components\InputString;
use Deka\Components\View\Components\InputText;
use Illuminate\Support\ServiceProvider;

class ComponentsServiceProvider extends ServiceProvider
{
    public function boot()
    {
        if ($this->app->runningInConsole()) {
            $this->publishes([
                __DIR__ . '/../resources/views' => base_path('resources/views/vendor/components'),
            ], 'views');

            $this->publishes([
                __DIR__ . '/../resources/assets' => public_path('vendor/components'),
            ], 'assets');
        }

        $this->loadTranslationsFrom(__DIR__. '/../resources/lang', 'dcomp');

        $this->loadViewsFrom(__DIR__ . '/../resources/views', 'components');

        $this->loadViewComponentsAs('dcomp', [
            'box' => 'components::components.box',
            'select' => 'components::components.select',
            'submit-buttons' => 'components::components.submit-buttons',
            'table' => 'components::components.table',
            'checker' => Checker::class,
            'scopeselect' => 'components::components.scopeselect',
            'multiselect' => 'components::components.multiselect',
            'conditional' => 'components::components.conditional',
            'imageselect' => 'components::components.image-select',
            'input-string' => InputString::class,
            'input-checkbox' => InputCheckbox::class,
            'input-text' => InputText::class,
            'input-html' => InputHtml::class,
            'input-date' => 'components::components.input-date',
        ]);

    }

    public function register()
    {
//        $this->mergeConfigFrom(__DIR__ . '/../config/components.php', 'components');
    }

}
