/**
 * We'll load the axios HTTP library which allows us to easily issue requests
 * to our Laravel back-end. This library automatically handles sending the
 * CSRF token as a header based on the value of the "XSRF" token cookie.
 */

window.axios = require('axios');

window.axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';

/**
 * Next we will register the CSRF Token as a common header with Axios so that
 * all outgoing HTTP requests automatically have it attached. This is just
 * a simple convenience so we don't have to attach every token manually.
 */

let token = document.head.querySelector('meta[name="csrf-token"]');

if (token) {
    window.axios.defaults.headers.common['X-CSRF-TOKEN'] = token.content;
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': token.content
        }
    });
} else {
    console.error('CSRF token not found: https://laravel.com/docs/csrf#csrf-x-csrf-token');
}

require('./jquery-ui.min');

$(document).ready(function() {

    var fixHelperModified = function(e, tr) {
            var $originals = tr.children();
            var $helper = tr.clone();
            $helper.children().each(function(index) {
                $(this).width($originals.eq(index).width())
            });
            return $helper;
        },
        updateAndStoreIndex = function(e, ui) {
            var no, route;
            route = ui.item.closest('table.sort').data('route');

            $('td.index', ui.item.parent()).each(function (i) {
                no = i+1;
                $(this).html(no);
                fid = $(this).attr('fid');
                storeOrdering(fid, no, route);
            });
        },
        storeOrdering = function(id, ordering, route){
            $.post( route,
                {
                    id: id,
                    ordering: ordering
                });
        },
        updateIndex = function(e, ui) {
            var no;
            $('td input', ui.item.parent()).each(function (i) {
                no = i+1;
                $(this).val(no);
            });
        },
        updateDeepIndex = function(e, ui) {
            var fid, no, route;
            route = ui.item.closest('table.sort-deep').data('route');

            $('td.index', ui.item.parent()).each(function (i) {
                no = i+1;
                $(this).html(no);
                fid = $(this).attr('fid');
                storeOrdering(fid, no, route);
            });
            $('td.subindex', ui.item.parent()).each(function (i) {
                no = i+1;
                $(this).html('- ' + no);
                fid = $(this).attr('fid');
                storeOrdering(fid, no, route);
            });
        };

    $(".sort tbody").each(function(){
        $(this).sortable({
            helper: fixHelperModified,
            stop: updateAndStoreIndex
        }).disableSelection();
    });

    //without updating the dbase
    $(".sort-only tbody").each(function(){
        $(this).sortable({
            helper: fixHelperModified,
            stop: updateIndex
        }).disableSelection();
    });

    $(".sort-deep tbody").each(function(){
        $(this).sortable({
            helper: fixHelperModified,
            stop: updateDeepIndex
        }).disableSelection();
    });

});
