import DVEditor from './DVEditor.vue';
import {createApp} from "vue";

window.renderEditor = function(id, name, html, showPlain = false) {
    let app = createApp({
        components: {DVEditor},
        data: () => ({
            name: name,
            html: html,
            hasPlain: showPlain
        }),
        template: '<DVEditor :name="name" :html="html" :hasplain="hasPlain"></DVEditor>'
    });

    app.mount(id)
}