<div class="icheck-primary">
        <input name="{{ $name }}" id="{{ $name }}" type="checkbox"{{ $value > 0? ' checked' : '' }} />
        <label for="{{ $name }}">{{ $label }}</label>
</div>

@section('css')
        @once
                <link href="https://cdnjs.cloudflare.com/ajax/libs/icheck-bootstrap/3.0.1/icheck-bootstrap.min.css" rel="stylesheet" />
        @endonce
@append
