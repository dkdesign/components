@props(['title', 'id' => '', 'class' => false, 'collapsible' => true])
@php
    $id = Str::slug($title);
@endphp
<div class="card {{ $class }}" id="{{ $id }}">
    <div class="card-header bg-light">
        {{ $title }}
        @if($collapsible)
            <div class="float-right" data-toggle="collapse" data-target="#card-body-{{ $id }}"><i class="fas fa-caret-down"></i> </div>
        @endif
    </div>
    <div class="card-body collapse show" id="card-body-{{ $id }}">
        {{ $slot }}
    </div>
</div>
