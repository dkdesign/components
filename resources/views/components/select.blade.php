@props(['options', 'label', 'name', 'required' => false, 'showErrors' => false, 'errors' => [], 'selected' => '', 'select2' => true, 'vmodel' => false])

@if($showErrors)
    @php
        $errorname = str_replace('[', '', $name);
        $errorname = str_replace(']', '', $errorname);
    @endphp
    <div class="form-group">
        <label for="{{ $name }}">{{ $label }}{{ $required? '*' : '' }}</label>
        <select @if($vmodel) v-model="{{ $vmodel }}" @endif class="form-control{{ ($errors && $errors->has($errorname))? ' is-invalid' : '' }}" id="{{ $name }}" name="{{ $name }}" style="width: 100%">
            @foreach($options as $val => $opt)
                <option value="{{ $val }}"{{ ($val == $selected)? ' selected=selected' : '' }}>{{ $opt }}</option>
            @endforeach
        </select>
        @if($errors && $errors->has($errorname))
            <p class="invalid-feedback">
                {{ __($errors->first($errorname)) }}
            </p>
        @endif
    </div>
@else
    <div class="form-group">
        <label for="{{ $name }}">{{ $label }}{{ $required? '*' : '' }}</label>
        <select @if($vmodel) v-model="{{ $vmodel }}" @endif class="form-control" id="{{ $name }}" name="{{ $name }}" style="width: 100%">
            @foreach($options as $val => $opt)
                <option value="{{ $val }}"{{ ($val == $selected)? ' selected=selected' : '' }}>{{ $opt }}</option>
            @endforeach
        </select>
    </div>
@endif

@if($select2)
@section('js')
    <script>
        $('#{{ $name }}').select2({
          minimumResultsForSearch: 5
        });
    </script>
@append
@endif
