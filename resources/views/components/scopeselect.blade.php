@props(['routename', 'selected', 'scopes', 'label'])

<div class="form-inline">
    <div class="form-group">
        <label class="my-1 mr-2" for="scopeselector">{{ ucfirst($label) }}</label>
        <select class="form-control" id="scopeselector" name="scopeselector">
            @foreach($scopes as $val => $opt)
            <option value="{{ $val }}"{{ ($val == $selected)? ' selected=selected' : '' }}>{{ $opt }}</option>
            @endforeach
        </select>
    </div>
</div>
<div class="clearfix mb-4"></div>

@section('js')
<script>
  $('#scopeselector').change(function () {
    var $this = $(this);
    window.location.replace("{{ $routename }}"+$this.val());
  });
</script>
@append
