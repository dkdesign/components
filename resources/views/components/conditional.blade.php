<div class="mt-4 mb-3">
    <div class="icheck-peterriver">
        <input name="{{ $name }}" id="{{ $name }}" type="checkbox"{{ $value > 0? ' checked' : '' }} />
        <label for="{{ $name }}">{{ $label }}</label>
    </div>
</div>
<div id="{{$name}}-cond" class="{{ $value > 0? ' ' : 'd-none' }}">
    {{ $slot }}
</div>
@if(isset($negative))
    <div id="{{$name}}-cond-neg" class="{{ $value < 1? ' ' : 'd-none' }}">
        {{ $negative }}
    </div>
@endif

    @section('css')
        @once
            <link href="https://cdnjs.cloudflare.com/ajax/libs/icheck-bootstrap/3.0.1/icheck-bootstrap.min.css"
                  rel="stylesheet"/>
        @endonce
    @append

    @section('js')
        <script>
            document.addEventListener("DOMContentLoaded", function () {
                var input{{ $name }} = document.querySelector("input[name={{ $name }}]")
                var condition{{ $name }} = document.getElementById("{{ $name }}-cond")
                var conditionRev{{ $name }} = document.getElementById("{{ $name }}-cond-neg")

                input{{ $name }}.addEventListener("change", () => {
                    if (input{{ $name }}.checked) {
                        condition{{ $name }}.classList.remove("d-none")
                        conditionRev{{ $name }}.classList.add("d-none")
                    } else {
                        condition{{ $name }}.classList.add("d-none")
                        conditionRev{{ $name }}.classList.remove("d-none")
                    }
                });
            });
        </script>
    @append
