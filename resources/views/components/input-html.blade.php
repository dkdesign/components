@php
    $nameId = Illuminate\Support\Str::slug($name).'-container';
@endphp

<div class="form-group">
    <label for="{{ $name }}" class="control-label">{{ $label }}{{ $required? '*' : '' }}</label>
    <div id="{{ $nameId }}">
    </div>
    @if($errors && $errors->has($name))
        <p class="invalid-feedback">
            {{ __($errors->first($name)) }}
        </p>
    @endif
</div>

@section('js')
    @once
        <script src="{{ asset('vendor/components/js/html_editor.js') }}"></script>
    @endonce
    <script>
        $(document).ready(function () {
            window.renderEditor('#{{ $nameId }}', "{{ $name }}", @json($value), {{ $showPlain? "true" : "false" }})
        })
    </script>
@append
