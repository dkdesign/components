<div class="form-group">
    <input type="submit" value="{{ ucfirst(__('dcomp::dcomp.submit_close')) }}" name="submitclose" class="btn btn-primary">
    <input type="submit" value="{{ ucfirst(__('dcomp::dcomp.submit')) }}" name="submit" class="btn btn-success">
    @if(count($extraButtons))
        @foreach($extraButtons as $button)
            <a class="btn btn-{{ $button['class'] }}" href="{{ $button['href'] }}">{{ $button['label'] }}</a>
        @endforeach
    @endif
</div>
