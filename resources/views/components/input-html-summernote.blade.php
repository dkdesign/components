@php
    $nameId = Illuminate\Support\Str::slug($name).'-container';
@endphp

<div class="form-group">
    <label for="{{ $name }}" class="control-label">{{ $label }}{{ $required? '*' : '' }}</label>
    <div id="{{ $nameId }}">
    </div>
    @if($errors && $errors->has($name))
        <p class="invalid-feedback">
            {{ __($errors->first($name)) }}
        </p>
    @endif
</div>

@section('css')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.9.1/summernote-bs4.min.css" />
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.2/dist/css/bootstrap.min.css"/>
@append

@section('js')
    @once
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/2.9.2/umd/popper.min.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.2/dist/js/bootstrap.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.9.1/summernote-bs4.min.js"></script>
        <script src="{{ asset('vendor/components/js/summernote-onpaste.js') }}"></script>
        <script src="{{ asset('vendor/components/js/html_editor2.js') }}"></script>
    @endonce
    <script>
        new Vue({
            el: '#{{ $nameId }}',
            data: {
                name: '{{ $name }}',
                text: `{!! $value !!}`,
                readOnly: {{ $readOnly? "true" : "false" }},
                config: @json($summernoteConfig)
            },
            template: '<SummerNote name="{{ $name }}" :value="text" :config="config" :read-only="readOnly" />'
        });
    </script>
@append
