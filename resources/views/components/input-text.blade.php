@if($showErrors)
    @php
        $errorname = str_replace('[', '.', $name);
        $errorname = str_replace(']', '', $errorname);
    @endphp
    <div class="form-group {{ ($errors && $errors->has($errorname))? 'has-error' : '' }}">
        <label for="{{ $name }}" class="control-label">{{ $label }}{{ $required? '*' : '' }}</label>
        <textarea name="{{ $name }}" id="{{ $name }}" class="form-control">{{ $value }}</textarea>
        @if($errors && $errors->has($errorname))
            <p class="help-block">
                {{ $errors->first($errorname) }}
            </p>
        @endif
    </div>
@else
    <div class="form-group">
        <label for="{{ $name }}" class="control-label">{{ $label }}{{ $required? '*' : '' }}</label>
        <textarea name="{{ $name }}" id="{{ $name }}" class="form-control">{{ $value }}</textarea>
    </div>
@endif
