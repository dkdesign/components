@props(['name', 'label', 'required' => false, 'value' => '', 'showErrors' => false, 'errors' => false, 'format' => 'yyyy-MM-dd', 'loaded' => false])
@php
    $errorname = str_replace('[', '.', $name);
    $errorname = str_replace(']', '', $errorname);
@endphp

<div class="form-group">
    <label for="{{ $name }}" class="control-label">{{ $label }}{{ $required? '*' : '' }}</label>
    <div id="{{ $name }}">
    </div>
    @if($showErrors && $errors->has($errorname))
        <p class="invalid-feedback">
            {{ __($errors->first($errorname)) }}
        </p>
    @endif
</div>

@section('js')
    @once
        <script src="https://cdn.jsdelivr.net/npm/vue@^2"></script>
        <script src="https://unpkg.com/vuejs-datepicker"></script>
        <script src="https://unpkg.com/vuejs-datepicker/dist/locale/translations/{{ config('app.locale', 'en') }}.js"></script>
    @endonce
    <script>
        var locale = "{{ config('app.locale', 'en') }}";
        new Vue({
            el: `#`+ '{{ $name }}',
            components: {vuejsDatepicker},
            data: () => ({
                name: '',
                language: {},
                value: '',
            }),
            created() {
                this.name = "{{ $name }}";
                this.value = "{{ $value }}";
                this.language = eval(`vdp_translation_${locale}.js`);
            },
            template: '<vuejs-datepicker format="{{ $format }}" :name="name" :language="language" v-model="value" clear-button="true" clear-button-icon="fa fa-times" class="form-control{{ ($errors && $errors->has($errorname))? ' is-invalid' : '' }}"/>'
        });
    </script>
@append

@section('css')
    <style>
        .vdp-datepicker input {
            display: block;
            width: 100%;
            height: calc(2.25rem);
            padding: .375rem .75rem;
            font-size: 1rem;
            font-weight: 400;
            line-height: 1.5;
            color: #495057;
            background-color: #fff;
            background-clip: padding-box;
            border: none;
            box-shadow: inset 0 0 0 transparent;
        }

        .vdp-datepicker.form-control {
            padding: 0 4px;
        }
    </style>
@append
