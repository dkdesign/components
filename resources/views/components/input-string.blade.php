@if($showErrors)
    @php
        $errorname = str_replace('[', '.', $name);
        $errorname = str_replace(']', '', $errorname);
    @endphp
    <div class="form-group" id="{{ $pid }}-field">
        <label for="{{ $pid }}" class="control-label">{{ $label }}{{ $required? '*' : '' }}</label>
        @if($pre || $post)
            <div class="input-group mb-3">
                @endif
                @if($pre)
                    <div class="input-group-prepend">
                        <span class="input-group-text">{{ $pre }}</span>
                    </div>
                @endif
                <input type="{{ $type }}" name="{{ $name }}" id="{{ $pid }}" class="form-control{{ ($errors && $errors->has($errorname))? ' is-invalid' : '' }}" value="{{ $value }}" {{ $required? 'required' : '' }}/>
                @if($post)
                    <div class="input-group-append">
                        <span class="input-group-text">{{ $post }}</span>
                    </div>
                @endif
                @if($pre || $post)
            </div>
        @endif
        @if($errors && $errors->has($errorname))
            <p class="invalid-feedback">
                {{ __($errors->first($errorname)) }}
            </p>
        @endif
    </div>
@else
    <div class="form-group" id="{{ $pid }}-field">
        <label for="{{ $pid }}" class="control-label">{{ $label }}{{ $required? '*' : '' }}</label>
        @if($pre || $post)
            <div class="input-group mb-3">
                @endif
                @if($pre)
                    <div class="input-group-prepend">
                        <span class="input-group-text">{{ $pre }}</span>
                    </div>
                @endif
                <input type="{{ $type }}" name="{{ $name }}" id="{{ $pid }}" class="form-control" value="{{ $value }}" {{ $required? 'required' : '' }}/>
                @if($post)
                    <div class="input-group-append">
                        <span class="input-group-text">{{ $post }}</span>
                    </div>
                @endif
                @if($pre || $post)
            </div>
        @endif
    </div>
@endif
