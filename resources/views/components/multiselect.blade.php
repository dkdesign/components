@props(['options', 'label', 'name', 'selected' => [], 'required' => false, 'showErrors' => false, 'errors' => []])

@if($showErrors)
    @php
        $errorname = str_replace('[', '', $name);
        $errorname = str_replace(']', '', $errorname);
    @endphp
    <div class="form-group">
        <label for="{{ $name }}">{{ $label }}{{ $required? '*' : '' }}</label>
        <select class="select2 form-control{{ ($errors && $errors->has($errorname))? ' is-invalid' : '' }}" id="{{ $name }}" name="{{ $name }}" multiple="multiple">
            @foreach($options as $val => $opt)
                <option value="{{ $val }}"{{ in_array($val, $selected)? ' selected' : '' }}>{{ $opt }}</option>
            @endforeach
        </select>
        @if($errors && $errors->has($errorname))
            <p class="invalid-feedback">
                {{ __($errors->first($errorname)) }}
            </p>
        @endif
    </div>
@else
    <div class="form-group">
        <label for="{{ $name }}">{{ $label }}{{ $required? '*' : '' }}</label>
        <select class="select2 form-control" id="{{ $name }}" name="{{ $name }}" multiple="multiple">
            @foreach($options as $val => $opt)
                <option value="{{ $val }}"{{ in_array($val, $selected)? ' selected' : '' }}>{{ $opt }}</option>
            @endforeach
        </select>
    </div>
@endif

@section('js')
    <script>
      $('select[name="{{ $name }}"]').select2({
        placeholder: "{{ ucfirst(__('dcomp::dcomp.make_choice')) }}",
        minimumResultsForSearch: 5
      });
    </script>
@append
