@props(['id' => '', 'class' => false, 'sortable' => false, 'route' => ''])
@php
    if ($sortable) {
        $class = ($class)? $class . ' sort' : 'sort';
    }
@endphp

<table class="table {{ $class }}" id="{{ $id }}" data-route="{{ $route }}">
    <thead>
    {{ $thead }}
    </thead>
    <tbody>
    {{ $slot }}
    </tbody>
</table>

@if($sortable)
@section('js')
    @once
        <script src="{{ asset('vendor/components/js/table-sort.js') }}"></script>
    @endonce
@append
@endif
