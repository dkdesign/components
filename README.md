# Laravel components by Deka Webdesign

## Installation

You can install the package via composer:

```bash
composer require deka/components
```

You MUST publish the assets with:
```bash
php artisan vendor:publish --provider="Deka\Components\ComponentsServiceProvider" --tag="assets"
```

You can publish the views for finetuning with:
```bash
php artisan vendor:publish --provider="Deka\Components\ComponentsServiceProvider" --tag="views"
```

## Usage
Just like any other component in laravel, but with the namespace 'dcomp'.
In Blade this could look like:

``` php
<x-dcomp-input-string/>
```

## Table
This one needs some explanation.

The component has a named slot 
``` php
<x-slot name="thead">
 ```
in which you can place the header columns. Below that you simply place the table rows. 
If you want this table to be sortable it requires certain rules:
- a POST route should be added, that takes in an id and ordering for the object to be resorted.
- Within each row there should be a td with the class 'index', and an attribute 'fid' with the id of the item

## Available components
``` php
<x-dcomp-box/>
<x-dcomp-checker/>
<x-dcomp-image-select/>
<x-dcomp-input-checkbox/>
<x-dcomp-input-date/>
<x-dcomp-input-html/>
<x-dcomp-input-string/>
<x-dcomp-input-text/>
<x-dcomp-input-checkbox/>
<x-dcomp-multiselect/>
<x-dcomp-scopeselect/>
<x-dcomp-select/>
<x-dcomp-submitbuttons/>
<x-dcomp-table/>
```
