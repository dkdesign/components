const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix
    .js('src/js/html_editor.js', 'resources/assets/js').vue({version: 3})
    // .js('src/js/html_editor2.js', 'resources/assets/js')
    // .js('src/js/table-sort.js', 'resources/assets/js')
    .options({
        processCssUrls: false
    })
    .copy('resources/assets/js', '../../cms-test/public/vendor/components/js')
    .copy('resources/assets/js', '../bcf8/public/vendor/components/js')
    // .copy('resources/assets/js', '../../mmv/public/vendor/components/js')
// .copy('images', '../bcf8/public/images')
// .sass('resources/sass/app.scss', 'public/css')
;
